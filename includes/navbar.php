
  <!-- Header section -->
  <header class="header-section">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 text-center text-lg-left">
            <!-- logo -->
            <a href="./index.html" class="site-logo">
              <img src="img/logo.png" alt="">
            </a>
          </div>
          <div class="col-xl-6 col-lg-5">
            <form class="header-search-form" action="search.php" method="POST">
              <input type="text" name="valueToSearch" placeholder="Hledat název nebo značku">
              <button><i class="flaticon-search"></i></button>
            </form>
          </div>
          </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="main-navbar">
      <div class="container">
        <!-- menu -->
        <ul class="main-menu">
          <li><a href="masoaryby.php">Maso a ryby</a></li>
          <li><a href="mlecneachlazene.php">Mléčné a chlazené</a></li>
          <li><a href="trvanlive.php">Trvanlivé</a></li>
          <li><a href="napoje.php">Nápoje</a></li>
          <li><a href="compare.php">Porovnávač</a></li>
          <li><a href="seznam.php">Nákupní seznam</a></li>
        </ul>

      </div>
    </nav>
  </header>
  <!-- Header section end -->