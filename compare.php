<?php
include 'includes/conn.php'; 
$pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
$pdo->query("SET NAMES 'utf8'");
$sql =  "SELECT * FROM products WHERE store = 'rohlik.cz'";
$q = $pdo->query($sql);
$q->setFetchMode(PDO::FETCH_ASSOC);

$rohlik = array();

 
 while ($row = $q->fetch())
{
    array_push($rohlik, $row["name"]);
}
$sql1 = "SELECT * FROM products WHERE store = 'kosik.cz'";
$q = $pdo->query($sql1);
$q->setFetchMode(PDO::FETCH_ASSOC);
$kosik = array();

while ($row = $q->fetch())
{
    array_push($kosik, $row["name"]);

}


function isInArray($array, $searchIn) {
    static $inc = array();
    
    foreach ($array as $v) {
        if (is_array($v) && sizeof($v) > 0 ) {
            isInArray($v, $searchIn);
            

        } else if (!is_array($v) && in_array($v, $searchIn)) {
            array_push($inc, $v);
        }
    }
    
    return $inc;
}

$totals = isInArray($rohlik, $kosik);


?>
<?php include 'includes/header.php'; ?>
<body>

<?php include 'includes/navbar.php'; ?>


	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>porovnávač</h4>
		</div>
	</div>
	<!-- Page info end -->

		<!-- Category section -->
	<section class="category-section spad">
		<div class="container">
			<div class="col-lg-9  order-1 order-lg-2 mb-5 mb-lg-0">
				<div class="row">
               		<?php foreach ($totals as $value){
               		$sql2 = "SELECT * FROM `products` WHERE name LIKE :value AND store = 'rohlik.cz'";
               		$sql3 = "SELECT * FROM `products` WHERE name LIKE :value AND store = 'kosik.cz'";      
               		$q = $pdo->prepare($sql2);
    				$q->execute([':value' =>'%'.$value.'%']);
    				$q->setFetchMode(PDO::FETCH_ASSOC);
    				while ($row = $q->fetch()){
    					$priceR = $row["price"];
					?>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo $row["urlpic"]; ?>" alt="">
								</div>
									<div class="pi-text">
										<h6>Rohlik.cz: <?php echo $priceR; ?> Kč</h6>
										<?php }
										$q = $pdo->prepare($sql3);
    									$q->execute([':value' =>'%'.$value.'%']);
    									$q->setFetchMode(PDO::FETCH_ASSOC);
    									while ($row = $q->fetch()){
    										$priceK = $row["price"];
										?>
										<h6>Kosik.cz: <?php echo $priceK; ?> Kč</h6>
										<?php
										}
										if ($priceR > $priceK) { ?>
											<h6>Nejnižší cena: <?php echo $priceK; ?> Kč</h6>
										<?php
										}
										else{ ?>
											<h6>Nejnižší cena: <?php echo $priceR; ?> Kč</h6>
										<?php
										}
										?>
										<p><?php echo $row["name"]; ?></p>
									</div>
							</div>
						</div>
						<?php 
						}?>
				</div>
			</div>
		</div>
	</section>
	<!-- Category section end -->
<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
