<?php
session_start();
include 'includes/conn.php'; 
$pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
$pdo->query("SET NAMES 'utf8'");
$sql = "SELECT * FROM products WHERE type = 'trvanlive' ORDER BY price";
$q = $pdo->query($sql);
$q->setFetchMode(PDO::FETCH_ASSOC);
?>
<?php include 'includes/header.php'; ?>
<body>

<?php include 'includes/navbar.php'; ?>


	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>trvanlivé</h4>
		</div>
	</div>
	<!-- Page info end -->


	<!-- Category section -->
	<section class="category-section spad">
		<div class="container">
				<div class="col-lg-9  order-1 order-lg-2 mb-5 mb-lg-0">
					<div class="row">
					<?php while ($row = $q->fetch()): ?>
						<div class="col-lg-4 col-sm-6">
							<div class="product-item">
								<div class="pi-pic">
									<img src="<?php echo $row["urlpic"]; ?>" alt="">
								</div>
								<div class="pi-text">
									<h6><?php echo $row["store"]; ?>: <?php echo $row["price"]; ?> Kč</h6>
									<p><?php echo $row["name"]; ?></p>
								</div>
							</div>
						</div>
						<?php endwhile;?>
					</div>
				</div>
			</div>
	</section>
	<!-- Category section end -->




	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
