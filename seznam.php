<?php include 'includes/header.php'; ?>
<body>

<?php include 'includes/navbar.php'; ?>


	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>Nákupní seznam</h4>
		</div>
	</div>
	<!-- Page info end -->

		<!-- Category section -->
	<section class="category-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 order-2 order-lg-1">
					<div class="filter-widget"></div>
				</div>
			</div>
				<div class="form-group">
				    <form action="vypisSeznamu.php" method="POST">
						<textarea class="form-control" name="seznam" id="exampleFormControlTextarea3" rows="7" placeholder="Zde můžeš začít psát nákupní seznam. Každý produkt oddělujte čárkou."></textarea>
						<button class="btn btn-deep-orange">Hledat</button>
					</form>
				</div>				
		</div>
	</section>

	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script src="js/main.js"></script>
	</body>
</html>
